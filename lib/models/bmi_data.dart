// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';
import 'dart:math';

class BmiData {
  double weight;
  double height;
  DateTime logTime;
  BmiData({
    required this.weight,
    required this.height,
    required this.logTime,
  });
  BmiData.newBmi({
    required this.weight,
    required this.height,
  }) : logTime = DateTime.now();

  double get bmi => weight / pow(height / 100, 2);

  BmiData copyWith({
    double? weight,
    double? height,
    DateTime? logTime,
  }) {
    return BmiData(
      weight: weight ?? this.weight,
      height: height ?? this.height,
      logTime: logTime ?? this.logTime,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'weight': weight,
      'height': height,
      'logTime': logTime.millisecondsSinceEpoch,
    };
  }

  factory BmiData.fromMap(Map<String, dynamic> map) {
    return BmiData(
      weight: map['weight'] as double,
      height: map['height'] as double,
      logTime: DateTime.fromMillisecondsSinceEpoch(map['logTime'] as int),
    );
  }

  String toJson() => json.encode(toMap());

  factory BmiData.fromJson(String source) =>
      BmiData.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() =>
      'BmiData(weight: $weight, height: $height, logTime: $logTime)';

  @override
  bool operator ==(covariant BmiData other) {
    if (identical(this, other)) return true;

    return other.weight == weight &&
        other.height == height &&
        other.logTime == logTime;
  }

  @override
  int get hashCode => weight.hashCode ^ height.hashCode ^ logTime.hashCode;
}
