import 'package:bmi/bmi_list_screen.dart';
import 'package:bmi/services/bmi_data_service.dart';
import 'package:bmi/services/user_service.dart';
import 'package:bmi/welcome_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';

import 'models/bmi_data.dart';
import 'models/user.dart';

List bmiSuggestions = [
  {
    "text": "<18.5",
    "image": "assets/images/under_weight.png",
    "suggest":
        "A BMI of under 18.5 indicates that a person has insufficient weight, so they may need to put on some weight. They should ask a doctor or dietitian for advice."
  },
  {
    "text": "18.5-24.9",
    "image": "assets/images/normal.png",
    "suggest":
        "A BMI of 18.5–24.9 indicates that a person has a healthy weight for their height. By maintaining a healthy weight, they can lower their risk of developing serious health problems."
  },
  {
    "text": "25–29.9",
    "image": "assets/images/over_weight.png",
    "suggest":
        "A BMI of 25–29.9 indicates that a person is slightly overweight. A doctor may advise them to lose some weight for health reasons. They should talk with a doctor or dietitian for advice."
  },
  {
    "text": ">30.0",
    "image": "assets/images/obese.png",
    "suggest":
        "A BMI of over 30 indicates that a person has obesity. Their health may be at risk if they do not lose weight. They should talk with a doctor or dietitian for advice."
  },
];
process(double bmi) {
  if (bmi < 18.5) {
    return bmiSuggestions[0];
  } else if (bmi < 25) {
    return bmiSuggestions[1];
  } else if (bmi < 30) {
    return bmiSuggestions[2];
  }
  return bmiSuggestions[3];
}

class BmiScreen extends StatefulWidget {
  final User user;

  const BmiScreen({Key? key, required this.user}) : super(key: key);

  @override
  _BmiScreenState createState() => _BmiScreenState();
}

class _BmiScreenState extends State<BmiScreen> {
  final _formKey = GlobalKey<FormState>();
  double bmi = 0.0;
  bool showResult = false;
  var result = bmiSuggestions[0];
  TextEditingController weightInput = TextEditingController();
  TextEditingController heightInput = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
          child: ListView(
        children: [
          DrawerHeader(
            padding: EdgeInsets.zero,
            decoration: const BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/images/bmi_women.jpg"),
                    fit: BoxFit.cover)),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Container(
                  width: double.infinity,
                  color: Colors.black.withOpacity(0.5),
                  child: Text(
                    "MY BMI",
                    style:
                        GoogleFonts.niramit(fontSize: 30, color: Colors.white),
                    textAlign: TextAlign.center,
                  ),
                )
              ],
            ),
          ),
          ListTile(
            title: Text(
              "BMI History",
              textAlign: TextAlign.right,
              style: GoogleFonts.niramit(
                  fontSize: 20, fontWeight: FontWeight.bold),
            ),
            onTap: () {
              Navigator.push(context, MaterialPageRoute(
                builder: (context) {
                  return BmiListScreen();
                },
              ));
            },
          ),
          ListTile(
            title: Text(
              "Reset",
              textAlign: TextAlign.right,
              style: GoogleFonts.niramit(
                  fontSize: 20, fontWeight: FontWeight.bold),
            ),
            onLongPress: () {
              UserService.clearUser().then((value) {
                Navigator.pushReplacement(context, MaterialPageRoute(
                  builder: (context) {
                    return WelcomeScreen();
                  },
                ));
              });
            },
          )
        ],
      )),
      appBar: AppBar(
        title: Text("MY BMI"),
        actions: [
          InkWell(
              onTap: () {
                Navigator.push(context, MaterialPageRoute(
                  builder: (context) {
                    return BmiListScreen();
                  },
                ));
              },
              child:
                  Padding(padding: EdgeInsets.all(16), child: Icon(Icons.list)))
        ],
      ),
      body: Container(
        padding: EdgeInsets.all(16),
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              Container(
                width: double.infinity,
                height: 70,
                color: const Color(0xFF1c9c01).withAlpha(128),
                child: Row(
                  children: [
                    Container(
                      padding: EdgeInsets.only(left: 16),
                      child: Text(
                        "Hello ${widget.user.name}",
                        style: GoogleFonts.niramit(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                    ),
                    Expanded(
                      child: Container(),
                    ),
                    Icon(
                      widget.user.gender == "M" ? Icons.man : Icons.woman,
                      size: 40,
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 16,
              ),
              TextFormField(
                inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                controller: weightInput,
                // autovalidateMode: AutovalidateMode.always,
                decoration: InputDecoration(
                    hintText: "Input your weight",
                    labelText: "Weight",
                    border: OutlineInputBorder()),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return "Weight is empty!";
                  }
                  var num = double.parse(value);
                  if (num < 20 || num > 250) {
                    return "Weight must between 20-250";
                  }
                  return null;
                },
              ),
              SizedBox(
                height: 16,
              ),
              TextFormField(
                controller: heightInput,
                inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                autovalidateMode: AutovalidateMode.always,
                decoration: InputDecoration(
                    hintText: "Input your height",
                    labelText: "Height",
                    border: OutlineInputBorder()),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return "Height is empty!";
                  }
                  var num = double.parse(value);
                  if (num < 20 || num > 250) {
                    return "Weight must between 20-250";
                  }
                  return null;
                },
              ),
              Container(
                padding: EdgeInsets.all(16),
                child: ElevatedButton(
                    onPressed: () {
                      if (_formKey.currentState!.validate()) {
                        var weight = double.parse(weightInput.text);
                        var height = double.parse(heightInput.text) / 100;
                        setState(() {
                          bmi = weight / (height * height);
                          result = process(bmi);
                          showResult = true;
                          BmiDataService.addBmi(
                              BmiData.newBmi(weight: weight, height: height));
                        });
                      }
                    },
                    child: Text("Calculate")),
              ),
              Visibility(
                visible: showResult,
                child: Expanded(
                  child: Container(
                    decoration: BoxDecoration(boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.2),
                        spreadRadius: 5,
                        blurRadius: 7,
                        offset:
                            const Offset(0, 3), // changes position of shadow
                      )
                    ]),
                    child: Expanded(
                      child: Row(
                        children: [
                          Expanded(
                              child: Column(
                            children: [
                              Container(
                                padding: const EdgeInsets.all(16.0),
                                child: Text(
                                  bmi.toStringAsFixed(1),
                                  style: const TextStyle(
                                      fontSize: 50,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              Container(
                                padding: const EdgeInsets.all(16.0),
                                child: Text(result["suggest"]),
                              ),
                            ],
                          )),
                          Image.asset(
                            result["image"],
                            fit: BoxFit.contain,
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
