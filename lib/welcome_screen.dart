import 'package:bmi/bmi_screen.dart';
import 'package:bmi/info_screen.dart';
import 'package:bmi/models/user.dart';
import 'package:bmi/services/user_service.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class WelcomeScreen extends StatefulWidget {
  const WelcomeScreen({Key? key}) : super(key: key);

  @override
  State<WelcomeScreen> createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  User? user;

  @override
  void initState() {
    super.initState();
    getUser();
  }

  Future<void> getUser() async {
    var userTmp = await UserService.loadUser();
    setState(() {
      user = userTmp;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(children: [
        Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          color: Colors.blue,
          child: Image.asset("assets/images/bmi_women.jpg", fit: BoxFit.cover),
        ),
        Container(
          color: Colors.black.withOpacity(0.5),
          margin:
              EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.7),
          padding: EdgeInsets.all(16),
          height: MediaQuery.of(context).size.height * 0.3,
          width: MediaQuery.of(context).size.width,
          child: Text(
              "Health is the greatest gift, contentment the greatest wealth, faithfulness the best relationship.",
              style: GoogleFonts.niramit(
                  color: Colors.white,
                  fontSize: 25,
                  fontWeight: FontWeight.bold),
              textAlign: TextAlign.center),
        ),
        Column(
          children: [
            Expanded(
              child: Container(),
            ),
            Container(
              height: 100,
              width: MediaQuery.of(context).size.width,
              child: Row(
                children: [
                  Expanded(
                    child: Container(),
                  ),
                  Container(
                    padding: EdgeInsets.all(16),
                    child: Text(
                      "BMI",
                      textAlign: TextAlign.right,
                      style: GoogleFonts.niramit(
                        color: Colors.white,
                        fontSize: 30,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      if (user == null) {
                        Navigator.pushReplacement(context, MaterialPageRoute(
                          builder: (context) {
                            return InfoScreen();
                          },
                        ));
                      } else {
                        Navigator.pushReplacement(context, MaterialPageRoute(
                          builder: (context) {
                            return BmiScreen(user: user!);
                          },
                        ));
                      }
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(25),
                        color: Theme.of(context).primaryColor,
                      ),
                      margin: EdgeInsets.all(16),
                      width: 60,
                      height: 60,
                      child: Icon(Icons.arrow_forward),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ]),
    );
  }
}
