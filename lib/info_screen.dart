import 'package:bmi/bmi_screen.dart';
import 'package:bmi/services/user_service.dart';
import 'package:flutter/material.dart';

import 'models/user.dart';

class InfoScreen extends StatefulWidget {
  const InfoScreen({Key? key}) : super(key: key);

  @override
  _InfoScreenState createState() => _InfoScreenState();
}

class _InfoScreenState extends State<InfoScreen> {
  final _formKey = GlobalKey<FormState>();
  var gender = "M";
  TextEditingController nameInput = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("MY BMI"),
        ),
        body: Container(
          padding: EdgeInsets.all(16),
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                Padding(
                  padding: EdgeInsets.only(bottom: 16),
                  child: TextFormField(
                    controller: nameInput,
                    autovalidateMode: AutovalidateMode.always,
                    decoration: InputDecoration(
                        hintText: "Input your name",
                        labelText: "Name",
                        border: OutlineInputBorder()),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return "Name is empty!";
                      }
                      if (value.length < 3) {
                        return "Name length must more than 3 characters.";
                      }
                      return null;
                    },
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          gender = "M";
                        });
                      },
                      child: Container(
                        decoration: BoxDecoration(
                            color: Theme.of(context)
                                .primaryColor
                                .withOpacity(gender == "M" ? 1.0 : 0.5),
                            borderRadius: BorderRadius.circular(10)),
                        width: MediaQuery.of(context).size.width * 0.3,
                        height: MediaQuery.of(context).size.width * 0.3,
                        child: Icon(
                          color: Colors.black
                              .withOpacity(gender == "M" ? 1.0 : 0.5),
                          Icons.man,
                          size: MediaQuery.of(context).size.width * 0.2,
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          gender = "F";
                        });
                      },
                      child: Container(
                        decoration: BoxDecoration(
                            color: Theme.of(context)
                                .primaryColor
                                .withOpacity(gender == "F" ? 1.0 : 0.5),
                            borderRadius: BorderRadius.circular(10)),
                        width: MediaQuery.of(context).size.width * 0.3,
                        height: MediaQuery.of(context).size.width * 0.3,
                        child: Icon(
                          Icons.woman,
                          color: Colors.black
                              .withOpacity(gender == "F" ? 1.0 : 0.5),
                          size: MediaQuery.of(context).size.width * 0.2,
                        ),
                      ),
                    ),
                  ],
                ),
                Container(
                  padding: EdgeInsets.all(16),
                  child: ElevatedButton(
                      onPressed: () {
                        if (_formKey.currentState!.validate()) {
                          Navigator.push(context, MaterialPageRoute(
                            builder: (context) {
                              UserService.saveUser(
                                  User(name: nameInput.text, gender: gender));
                              return BmiScreen(
                                  user: User(
                                      name: nameInput.text, gender: gender));
                            },
                          ));
                        }
                      },
                      child: Text("Next")),
                )
              ],
            ),
          ),
        ));
  }
}
