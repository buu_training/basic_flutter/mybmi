import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

import '../models/bmi_data.dart';

class BmiDataService {
  static List<BmiData> list = [];
  static Future<void> addBmi(BmiData bmi) async {
    var perfs = await SharedPreferences.getInstance();
    list = await getBmi();
    list.insert(0, bmi);
    print(list);
    perfs.setString("bmi", json.encode(list));
  }

  static Future<List<BmiData>> getBmi() async {
    var perfs = await SharedPreferences.getInstance();
    var strBmi = perfs.getString("bmi");
    if (strBmi == null) {
      return <BmiData>[];
    }
    var bmis = json.decode(strBmi).map<BmiData>((value) {
      return BmiData.fromJson(value);
    }).toList();
    print(bmis);
    return bmis;
  }
}
