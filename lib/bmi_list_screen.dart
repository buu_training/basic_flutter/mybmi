import 'package:bmi/services/bmi_data_service.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'models/bmi_data.dart';

class BmiListScreen extends StatefulWidget {
  const BmiListScreen({Key? key}) : super(key: key);

  @override
  _BmiListScreenState createState() => _BmiListScreenState();
}

class _BmiListScreenState extends State<BmiListScreen> {
  var bmiList = <BmiData>[];
  @override
  void initState() {
    super.initState();
    getBmiList();
  }

  Future<void> getBmiList() async {
    var value = await BmiDataService.getBmi();
    setState(() {
      bmiList = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Container(
        child: ListView.builder(
          itemCount: bmiList.length,
          itemBuilder: (context, index) {
            var data = bmiList[index];
            return ListTile(
              title: Text("${data.bmi.toStringAsFixed(1)}"),
              leading: getTrendIcon(index),
              subtitle: Text(
                  "${DateFormat('yyyy-MM-dd').format(data.logTime)}${data.weight} kg. ${data.height} cm."),
            );
          },
        ),
      ),
    );
  }

  Icon getTrendIcon(int index) {
    if (index == bmiList.length - 1 ||
        bmiList[index].bmi == bmiList[index + 1].bmi) {
      return Icon(Icons.trending_flat);
    } else if (bmiList[index].bmi > bmiList[index + 1].bmi) {
      return Icon(Icons.trending_up);
    }
    return Icon(Icons.trending_down);
  }
}
