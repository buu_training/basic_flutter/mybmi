import 'dart:convert';

import 'package:bmi/models/bmi_data.dart';

void main(List<String> args) {
  var bmiList = <BmiData>[
    BmiData.newBmi(weight: 81, height: 181),
    BmiData.newBmi(weight: 82, height: 181),
    BmiData.newBmi(weight: 83, height: 181),
  ];
  var b1 = BmiData.newBmi(weight: 81, height: 181);
  print("${b1.weight} ${b1.height} ${b1.logTime} ${b1.bmi}");

  var strJsonList = json.encode(bmiList);
  print(strJsonList);
  var bmis = json.decode(strJsonList).map((value) {
    return BmiData.fromJson(value);
  }).toList();
  print(bmis);
}
