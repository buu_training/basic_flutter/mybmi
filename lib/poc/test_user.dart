import 'dart:convert';

import '../models/user.dart';

void main(List<String> args) {
  var u1 = User(name: "Worawit", gender: "M");
  var u2 = User(name: "Jakkarin", gender: "M");
  print("${u1.name} ${u1.gender}");
  print("${u2.name} ${u2.gender}");

  print(u1.toJson());
  print(User.fromJson(u1.toJson()));
}
